# The TimeFlo Project: Design
*Author Name 2021*

## Introduction

*Introduce this document and the project it describes. You
should also summarize the remaining content of the document
here.*

## Architecture

*Describe the overall architecture of the proposed
implementation.*

*For each component identified in the architecture, describe
briefly what it will do, how it will work, and
implementation risks associated with it.*
