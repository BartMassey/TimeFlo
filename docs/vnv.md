# The TimeFlo Project: Validation and Verification
*Author Name 2021*

(*Thanks to Chris Gilmore for the document template for this
document.*)

## Introduction

*Introduce this document and the project it describes. You should also
summarize the remaining content of the document here.*

### Purpose and Scope

*You should describe this document by giving its purpose, scope.*

### Target Audience

*Describe the target audience for this document.*

### Terms and Definitions

*Define any terms or acronyms you will be using in the remainder of this
document.*

## V&amp;V Plan Description

*Describe the purpose of this section and outline its contents. Only a
few sentences are expected here.*

## Test Plan Description

*Describe the purpose of this section and outline its contents. Only a
few sentences are expected here.*

### Scope Of Testing

*Describe the scope of the V&amp;V plan. What areas of the
project will you be testing, and to what degree. Are there
any aspects you will not be testing? What kinds of tests
will you perform, and what kinds of tests will you omit?*

### Testing Schedule

*Outline your testing schedule here.*

### Release Criteria

*Describe your maximum fault tolerance and the criteria your system must
meet before being deployed.*

## Unit Testing

*Describe the purpose of this section and outline its contents. Name the
units you will be testing and describe their functionality.*

### Strategy

*Describe the strategy for unit testing of the project. Include items
such as code coverage, differences between unit and integration testing,
excluded code and why it's excluded, etc.*

### Test Cases

*Describe your test cases here.*

## System Testing

*Describe the purpose of this section and outline its contents. Only a
few sentences are expected here.*

### Test Cases

*Describe your test cases here.*

## Inspection

*Describe the strategy for inspection of the
project. Include items such as type of inspection, criteria
for inspection, inspection validation. Add as much detail as
needed here to be clear on what the inspection plan is.*
