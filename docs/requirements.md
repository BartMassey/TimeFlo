# The TimeFlo Project: Requirements Document
*Author Name 2021*

(*Thanks to Chris Gilmore for the document template for this
document.*)

## Introduction

*Introduce this document and the project it describes. You should also
summarize the remaining content of the document here.*

### Purpose and Scope

*You should describe this document by giving its purpose, scope.*

### Target Audience

*Describe the target audience for this document.*

### Terms and Definitions

*Define any terms or acronyms you will be using in the remainder of this
document.*

## Product Overview

*Give a high level description of the functionality of the project here.
Describe the purpose of this section. It may be useful to give your
definition of a user, a stake holder and a use case. If there are scope
limitations to the project, i.e. things you will not be doing, or are
not required to do, this is a good section to put those.*

### Users and Stakeholders

*Describe the purpose of this section. Only a few sentences are expected
here.*

#### *Stakeholder 1*

*List the first stakeholder or class of stakeholders if necessary.
Describe, exactly, their role in the development, deployment, use,
maintenance, etc. of the software.*

#### *Stakeholder 2*

*Etc.*

### Use cases

*Describe the purpose of this section. Only a few sentences are expected
here.*

#### *Use Case 1*

*Describe the first use case here. Be sure to explicitly identify the
participants, human or otherwise, and explain their roles.*

#### *Use Case 2*

*Etc.*

## Functional Requirements

*Describe the purpose of this section and outline its contents. Only a
few sentences are expected here. It may help to define a functional
requirement.*

### *Functional Requirements 1*

*Describe the first functional requirement. This is the meat of the
document, so be sure to use precise language. Include diagrams when
appropriate. Break this into a list of sub-requirements as needed.*

### *Functional Requirement 2*

*Etc.*

## Extra-functional Requirements

*Describe the purpose of this section and outline its contents. Only a
few sentences are expected here. It may help to define a nonfunctional
requirement.*

### *Extra-functional Requirement 1*

*Describe the first nonfunctional requirement. This is the meat of the
document, so be sure to use precise language. Include diagrams when
appropriate.*

### *Extra-functional Requirement 2*

*Etc.*
