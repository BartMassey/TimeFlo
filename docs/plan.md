# The TimeFlo Project: Project Plan
*Author Name 2021*

(*Thanks to Chris Gilmore for part of the document template
for this document.*)

## Resources

*List the SW tools, equipment, etc needed to execute this
project. Include programming language tools, build tools,
test tools, etc.*

## Work Breakdown

*List the low-level tasks needed to finish this project. A
good work breakdown will be in terms of small tasks: 1--4
effort hours each. Identify important dependencies between
tasks.*

## Schedule

*Give a schedule for completion of the milestones listed in
the next section. A table is a good structure for this.*

## Milestones and Deliverables

*Describe the purpose of this section and outline its
contents. Describe the milestones on a high level.*

### *Milestone/Deliverable 1*

*Explain what this milestone consists of. Identify the deliverable or
deliverables that will be produced at this milestone. Generally describe
what work will be done leading up to the conclusion of this milestone.*

### *Milestone/Deliverable 2*

*Etc.*
