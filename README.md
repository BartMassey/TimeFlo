# TimeFlo
Copyright &copy; 2021 *Author Full Name*

TimeFlo is an implementation of a
[Pomodoro&reg;](https://en.wikipedia.org/wiki/Pomodoro_Technique)-like
timer for breaking out of flow state.

*Description of your TimeFlo timer.*

## Status and Roadmap

*Status of the MVP. Describe what works and what
doesn't. Describe "future work" for this project.*

* [ ] Requirements complete.
* [ ] Project plan complete.
* [ ] Design complete.
* [ ] Implementation complete.
* [ ] Validation complete.

## Build and Run

*Instructions to build and run your project.*

## Development Docs

Development documentation is available for TimeFlo, including:

* [Requirements Specification](docs/reqs.md)
* [Project Plan](docs/plan.md)
* [Design Doc](docs/design.md)
* [V&amp;V Report](docs/vnv.md)
